# README #

Репозиторий для программ, представляемых для ознакомления с технологиями, в которых я разобрался и которые я применяю в своих проектах. Эти проекты не уникальны, большая часть их них написана по соответствующим тестовым заданиям и в соответствии с ними.

### Описание попроектно ###

* [Клиент ITunes](https://bitbucket.org/bander0s/test_exam/src/278c6122fbe7ee0f79c8c743d210c4968ea5e111/ITunesClient/?at=master) 
* [Клиент Github](https://bitbucket.org/bander0s/test_exam/src/278c6122fbe7ee0f79c8c743d210c4968ea5e111/Gihubusers/?at=master)

### How do I get set up? ###

* Используйте AndroidStudio/IntellIDEA c плагином для gradle v2.3.1  и gradle-3.3. для сборки проектов