package org.example.ghostage.itunesclient.injection;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import org.example.ghostage.itunesclient.data.SongRepository;
import org.example.ghostage.itunesclient.data.SongRepositoryImpl;
import org.example.ghostage.itunesclient.data.remote.ItunesSongRestService;

public class Injection {

    private static final String BASE_URL = "https://itunes.apple.com";
    private static OkHttpClient okHttpClient;
    private static ItunesSongRestService userRestService;
    private static Retrofit retrofitInstance;


    public static SongRepository provideUserRepo() {
        return new SongRepositoryImpl(provideItiunesSongRestService());
    }

    static ItunesSongRestService provideItiunesSongRestService() {
        if (userRestService == null) {
            userRestService = getRetrofitInstance().create(ItunesSongRestService.class);
        }
        return userRestService;
    }

    static OkHttpClient getOkHttpClient() {
        if (okHttpClient == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
            okHttpClient = new OkHttpClient.Builder().addInterceptor(logging).build();
        }

        return okHttpClient;
    }

    static Retrofit getRetrofitInstance() {
        if (retrofitInstance == null) {
            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                    .create();

            Retrofit.Builder retrofit = new Retrofit.Builder().client(Injection.getOkHttpClient()).baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create());
            retrofitInstance = retrofit.build();

        }
        return retrofitInstance;
    }
}
