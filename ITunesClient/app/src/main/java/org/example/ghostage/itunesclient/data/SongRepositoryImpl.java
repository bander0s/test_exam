package org.example.ghostage.itunesclient.data;


import java.io.IOException;
import java.util.List;

import rx.Observable;
import org.example.ghostage.itunesclient.data.remote.ItunesSongRestService;
import org.example.ghostage.itunesclient.data.remote.model.Song;
import org.example.ghostage.itunesclient.data.remote.model.SongsList;

/**
 * Реализация доступа к репозиторию песен
 */
public class SongRepositoryImpl implements SongRepository {
    private final static String TAG = "SongRepositoryImpl";
    /**Сервис для получения данных от скрвера REST*/
    private ItunesSongRestService itunesSongRestService;
    /***/
    public SongRepositoryImpl(ItunesSongRestService itunesSongRestService) {
        this.itunesSongRestService = itunesSongRestService;
    }

    /**
     * Поиск песни по указанной строке
     * @param searchTerm - строка запроса
     * @return
     */
    @Override
    public Observable<List<Song>> searchSongs(final String searchTerm) {
        return Observable.defer(() -> itunesSongRestService.searchITunesSongs(searchTerm)
                .concatMap(songsList -> Observable.from(songsList.getResults()).toList()));
    }

}
