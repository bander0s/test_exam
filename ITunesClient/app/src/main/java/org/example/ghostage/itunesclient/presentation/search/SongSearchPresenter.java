package org.example.ghostage.itunesclient.presentation.search;


import java.util.List;

import rx.Scheduler;
import rx.Subscriber;
import org.example.ghostage.itunesclient.data.SongRepository;
import org.example.ghostage.itunesclient.data.remote.model.Song;
import org.example.ghostage.itunesclient.presentation.base.BasePresenter;

/**
 * Реализация поиска песен и отработки ошибок если были. В нашем случае, если происходит
 * ошибка при поиске, она выводится на экран.
 */
class SongSearchPresenter extends BasePresenter<SongSearchContract.View> implements SongSearchContract.Presenter {
    private final static String TAG = "SongSearchPresenter";
    private final Scheduler mainScheduler, ioScheduler;
    private SongRepository songRepository;

    SongSearchPresenter(SongRepository songRepository, Scheduler ioScheduler, Scheduler mainScheduler) {
        this.songRepository = songRepository;
        this.ioScheduler = ioScheduler;
        this.mainScheduler = mainScheduler;
    }

    /**
     * Поисковый запрос
     * @param term - строка для поиска
     */
    @Override
    public void search(String term) {
        checkViewAttached();
        getView().showLoading();
        addSubscription(songRepository.searchSongs(term).subscribeOn(ioScheduler).observeOn(mainScheduler)
                .subscribe(new Subscriber<List<Song>>() {
                    /**
                     * Завершение процесса, на результаты, которого подписан класс
                     */
                    @Override
                    public void onCompleted() {

                    }

                    /**
                     * Отображение ошибки
                     * @param e - исключение
                     */
                    @Override
                    public void onError(Throwable e) {
                        getView().hideLoading();
                        getView().showError(
                                e.getMessage()); //TODO Отображаются ошибки и не предпринимается никаких попыток входа в систему. Вероятно, вы не хотите, чтобы эта ошибка отображалась для пользователей. Может потребоваться показать более дружественное сообщение :)
                    }

                    /**
                     * Отработка списка песен, полученных при обработке
                     * @param songs - список песен
                     */
                    @Override
                    public void onNext(List<Song> songs) {
                        getView().hideLoading();
                        getView().showSearchResults(songs);
                    }
                }));
    }
}
