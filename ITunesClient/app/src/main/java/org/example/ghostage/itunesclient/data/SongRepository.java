package org.example.ghostage.itunesclient.data;


import java.util.List;

import rx.Observable;

import org.example.ghostage.itunesclient.data.remote.model.Song;

/**
 * Интерфейс к репозиторию песен
 */
public interface SongRepository {

    Observable<List<Song>> searchSongs(String searchTerm);
}
