package org.example.ghostage.itunesclient.data.remote;


import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;
import org.example.ghostage.itunesclient.data.remote.model.Song;
import org.example.ghostage.itunesclient.data.remote.model.SongsList;

import java.util.List;

/**
 * Инлтерфейс для получения списка песен
 */
public interface ItunesSongRestService {

    @GET("/search?limit=50&country=ru&media=music")
    Observable<SongsList> searchITunesSongs(@Query("term") String searchTerm);

}
