package org.example.ghostage.itunesclient.presentation.search;


import java.util.List;

import org.example.ghostage.itunesclient.data.remote.model.Song;
import org.example.ghostage.itunesclient.presentation.base.MvpPresenter;
import org.example.ghostage.itunesclient.presentation.base.MvpView;

interface SongSearchContract {

    interface View extends MvpView {
        void showSearchResults(List<Song> itunesSongList);

        void showError(String message);

        void showLoading();

        void hideLoading();
    }

    interface Presenter extends MvpPresenter<View> {
        void search(String term);
    }
}
