package org.example.ghostage.itunesclient.presentation.search;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import org.example.ghostage.itunesclient.App;
import org.example.ghostage.itunesclient.R;
import org.example.ghostage.itunesclient.data.remote.model.Song;
import org.example.ghostage.itunesclient.injection.Injection;
import org.example.ghostage.itunesclient.presentation.player.MusicPlayer;
import org.example.ghostage.itunesclient.presentation.search.SongAdapter.ListCategoriesClickListener;

/**
 * Активити для ввода поискового запроса, отображения процесса
 * поиска, загрузки и отображения результата.
 */
public class SongSearchActivity extends AppCompatActivity implements SongSearchContract.View
        , Animation.AnimationListener, ListCategoriesClickListener {
    private final static String TAG = "SongSearchActivity";

    private SongSearchContract.Presenter userSearchPresenter;
    private SongAdapter songAdapter;
    private SearchView searchView;
    private Toolbar toolbar;
    private ProgressBar progressBar;
    private RecyclerView recyclerViewUsers;
    private TextView textViewErrorMessage;
    private int columns = 1;
    private SharedPreferences sysPrefs = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_search);

        sysPrefs = getSharedPreferences(TAG, Context.MODE_PRIVATE);
        App.viewMode = sysPrefs.getInt(App.PREF_VIEW_MODE, App.MODE_LIST);
        userSearchPresenter = new SongSearchPresenter(Injection.provideUserRepo(), Schedulers.io(),
                AndroidSchedulers.mainThread());
        userSearchPresenter.attachView(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        textViewErrorMessage = (TextView) findViewById(R.id.text_view_error_msg);
        recyclerViewUsers = (RecyclerView) findViewById(R.id.recycler_view_users);
        songAdapter = new SongAdapter(this);
        if(App.itunesTracks!=null){
            textViewErrorMessage.setVisibility(View.GONE);
            songAdapter.setItems(App.itunesTracks);
        }
        recyclerViewUsers.setAdapter(songAdapter);
        setupViewContent();
        Animation animToolbar = AnimationUtils.loadAnimation(this, R.anim.slide_in_up);
        animToolbar.setAnimationListener(this);
        toolbar.startAnimation(animToolbar);

    }

    /**
     * Настройка отображения списка песен
     */
    private void setupViewContent() {
        recyclerViewUsers.setHasFixedSize(true);

        // настраиваем тип отображения плиток в зависимости
        // от ориентации, выбранных настроек отображения и размеров экрана
        switch(App.viewMode){
            case App.MODE_LIST:
                columns = 1;
                break;
            case App.MODE_TABLE:
                columns = getResources().getInteger(R.integer.grid_columns);
                break;
        }
        recyclerViewUsers.setLayoutManager(
                (columns == 1) ? new LinearLayoutManager(this)
                        : new GridLayoutManager(this, columns));
    }

    @Override
    protected void onPause() {
        sysPrefs.edit().putInt(App.PREF_VIEW_MODE,App.viewMode).apply();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        userSearchPresenter.detachView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_user_search, menu);
        final MenuItem searchActionMenuItem = menu.findItem(R.id.menu_search);
        searchView = (SearchView) searchActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                if(query.length() < 5) {
                    Toast.makeText(getApplicationContext(), R.string.search_term_limitation, Toast.LENGTH_LONG);
                    menu.performIdentifierAction(R.id.menu_search, 0);
                }else{
                    userSearchPresenter.search(query);
                    toolbar.setTitle(query);
                    searchActionMenuItem.collapseActionView();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        searchActionMenuItem.expandActionView();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch(itemId){
            case R.id.menu_mode_list:
                App.viewMode = App.MODE_LIST;
                break;
            case R.id.menu_mode_table:
                App.viewMode = App.MODE_TABLE;
                break;
        }
        setupViewContent();
        item.setChecked(true);
        return super.onOptionsItemSelected(item);
    }

    /**
     * Показывает результат по найденным трекам
     * @param itunesSongList список треков
     */
    @Override
    public void showSearchResults(List<Song> itunesSongList) {
        App.itunesTracks.clear();
        App.itunesTracks.addAll(itunesSongList);
        recyclerViewUsers.setVisibility(View.VISIBLE);
        textViewErrorMessage.setVisibility(View.GONE);
        songAdapter.setItems(itunesSongList);
    }

    /**
     * Отображение сообщений об ошибках
     * @param message - сообщение об ошибок
     */
    @Override
    public void showError(String message) {
        textViewErrorMessage.setVisibility(View.VISIBLE);
        recyclerViewUsers.setVisibility(View.GONE);
        textViewErrorMessage.setText(message);
    }

    /**
     * Отображение процесса дагрузки при начале получения треков
     */
    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
        recyclerViewUsers.setVisibility(View.GONE);
        textViewErrorMessage.setVisibility(View.GONE);
    }

    /**
     * Скрытие процесса загрузки при завершении получения треков
     */
    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
        recyclerViewUsers.setVisibility(View.VISIBLE);
        textViewErrorMessage.setVisibility(View.GONE);

    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @Override
    public void onCategoryItemClick(int clickedItemIndex, View viewStart) {
        Intent intent = new Intent(this, MusicPlayer.class);
        intent.putExtra("Object", App.itunesTracks.get(clickedItemIndex));
        startActivity(intent);

    }
}
