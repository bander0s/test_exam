package org.example.ghostage.itunesclient.presentation.search;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import org.example.ghostage.itunesclient.R;
import org.example.ghostage.itunesclient.data.remote.model.Song;

/**
 * Адаптер для отображения списка писем
 */
class SongAdapter extends RecyclerView.Adapter<SongAdapter.SongViewHolder> /*implements*/{
    private final static String TAG = "SongAdapter";

    private Context context;
    private List<Song> items;

    static ListCategoriesClickListener mOnClickListener;

    SongAdapter(ListCategoriesClickListener listener) {
        mOnClickListener = listener;
    }

    /**
     * Конструктор на основе предоставленных данных
     * @param items - данные для отображения
     * @param context - контекст приложения
     */
    SongAdapter(List<Song> items, Context context) {
        this.items = items;
        this.context = context;
    }

    /**
     * Интерфейс для слушателя кликов по элементам
     */
    public interface ListCategoriesClickListener {
        void onCategoryItemClick(int clickedItemIndex, View viewStart);
    }

    /**
     * Создание контейнера элемента
     * @param parent - родительская группа видов
     * @param viewType - тип
     * @return
     */
    @Override
    public SongViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View v = LayoutInflater.from(context).inflate(R.layout.list_item_user, parent, false);

        return new SongViewHolder(v);
    }

    /**
     * Обработки после подключения группового элелемента
     * @param holder - элемент
     * @param position - позиция элемента
     */
    @Override
    public void onBindViewHolder(SongViewHolder holder, int position) {
        Song item = items.get(position);
        holder.position = position;
        holder.textViewTrackName.setText(item.getTrackName());
        holder.textViewArtistName.setText(item.getArtistName());
        Picasso.with(context).load(item.getArtworkUrl30()).into(holder.imageViewAvatar);
    }

    /**
     * Получение числа элементов
     * @return - число
     */
    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    void setItems(List<Song> itunesSongList) {
        this.items = itunesSongList;
        notifyDataSetChanged();
    }

    /**
     * Групповой элемент для реализации мест списка
     *
     */
    class SongViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
            Animation.AnimationListener {
        private Cursor mCursor;
        private Context mContext;
        private Animation slideBottom;

        private int position;

        final TextView textViewTrackName;
        final TextView textViewArtistName;
        final ImageView imageViewAvatar;

        /**
         * Конструктор элемента и всех видов внутри него
         * @param v - корневой вид элемента
         */
        SongViewHolder(View v ) {
            super(v);
            imageViewAvatar = (ImageView) v.findViewById(R.id.imageview_userprofilepic);
            textViewArtistName = (TextView) v.findViewById(R.id.textview_username);
            textViewTrackName = (TextView) v.findViewById(R.id.textview_user_profile_info);
            textViewTrackName.setOnClickListener(this);
            textViewArtistName.setOnClickListener(this);
            imageViewAvatar.setOnClickListener(this);
        }

        /**
         * Клик по элементу
         * @param v - вид
         */
        @Override
        public void onClick(View v) {
            int clickedCategory = getAdapterPosition();
            int idIndex;
            idIndex = this.position;
            mOnClickListener.onCategoryItemClick(position, v);
        }
        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {

        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    }
}