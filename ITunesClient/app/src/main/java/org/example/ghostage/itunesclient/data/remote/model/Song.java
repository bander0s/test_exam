package org.example.ghostage.itunesclient.data.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Currency;
import java.util.Random;

/**
 * Класс реализующий  отображение
 * элемента API поиска для ITunes
 * (используется c Retrofit) - песни
 */
public class Song implements Serializable{
    /**Тип обертки*/
    @SerializedName("wrapperType")
    @Expose
    private String wrapperType;
    /**разновидность*/
    @SerializedName("kind")
    @Expose
    private String kind;
    /**Идентификатор артиста*/
    @SerializedName("artistId")
    @Expose
    private Integer artistId;
    /**Идентификатор коллекции*/
    @SerializedName("collectionId")
    @Expose
    private Integer collectionId;
    /**Идентификатор трека*/
    @SerializedName("trackId")
    @Expose
    private Integer trackId;
    /**Название артиста*/
    @SerializedName("artistName")
    @Expose
    private String artistName;
    /**Название коллекции*/
    @SerializedName("collectionName")
    @Expose
    private String collectionName;
    /**Название трека*/
    @SerializedName("trackName")
    @Expose
    private String trackName;
    /***/
    @SerializedName("collectionCensoredName")
    @Expose
    private String collectionCensoredName;
    /***/
    @SerializedName("trackCensoredName")
    @Expose
    private String trackCensoredName;

    /**Артист аватарка*/
    @SerializedName("artistViewUrl")
    @Expose
    private String artistViewUrl;
    /**Коллекции обложка*/
    @SerializedName("collectionViewUrl")
    @Expose
    private String collectionViewUrl;
    /**Просмотр трека*/
    @SerializedName("trackViewUrl")
    @Expose
    private String trackViewUrl;
    /**адрес для доступа к предпросмотру*/
    @SerializedName("previewUrl")
    @Expose
    private String previewUrl;
    /**Иконка 30x30*/
    @SerializedName("artworkUrl30")
    @Expose
    private String artworkUrl30;
    /**Иконка 60x60*/
    @SerializedName("artworkUrl60")
    @Expose
    private String artworkUrl60;
    /**Иконка 100x100*/
    @SerializedName("artworkUrl100")
    @Expose
    private String artworkUrl100;
    /**Стоимость коллекции*/
    @SerializedName("collectionPrice")
    @Expose
    private Double collectionPrice;
    /**Стоимость трека*/
    @SerializedName("trackPrice")
    @Expose
    private Double trackPrice;
    /**Дата релиза*/
    @SerializedName("releaseDate")
    @Expose
    private String releaseDate;
    /***/
    @SerializedName("collectionExplicitness")
    @Expose
    private String collectionExplicitness;
    /***/
    @SerializedName("trackExplicitness")
    @Expose
    private String trackExplicitness;
    /**Количество дисков в коллекции*/
    @SerializedName("discCount")
    @Expose
    private Integer discCount;
    /**Номер диска*/
    @SerializedName("discNumber")
    @Expose
    private Integer discNumber;
    /**Число треков*/
    @SerializedName("trackCount")
    @Expose
    private Integer trackCount;
    /**Номер трека*/
    @SerializedName("trackNumber")
    @Expose
    private Integer trackNumber;
    /**Длина трека в милисекундах*/
    @SerializedName("trackTimeMillis")
    @Expose
    private Integer trackTimeMillis;
    /**Страна*/
    @SerializedName("country")
    @Expose
    private String country;
    /**Валюта*/
    @SerializedName("currency")
    @Expose
    private String currency;
    /**Имя первичного жанра*/
    @SerializedName("primaryGenreName")
    @Expose
    private String primaryGenreName;
    /**Потоковый контент*/
    @SerializedName("isStreamable")
    @Expose
    private Boolean isStreamable;

    public Song() {
    }
    /**Конструктор песни с указанным идентивикатором, имя коллекции, имя артиста и именем трека
     *

    public Song(final Integer trackId, final String trackName, final String artistName, final String collectionName) {
        this.trackId = trackId;
        this.trackName = trackName;
        this.artistName = artistName;
        this.collectionName = collectionName;
    }

    public Song(final Integer trackId) {
        this.trackId = trackId;
    }
     */
    /**
     * @return The wrapperType
     */
    public String getWrapperType() {
        return wrapperType;
    }

    /**
     * @return The kind
     */
    public String getKind() {
        return kind;
    }


    public String getArtistViewUrl() {
        return artistViewUrl;
    }


    public String getCollectionViewUrl() {
        return collectionViewUrl;
    }


    public String getTrackViewUrl() {
        return trackViewUrl;
    }


    public String getPreviewUrl() {
        return previewUrl;
    }


    public String getArtworkUrl30() {
        return artworkUrl30;
    }


    public String getArtworkUrl60() {
        return artworkUrl60;
    }


    public String getArtworkUrl100() {
        return artworkUrl100;
    }


    public Integer getArtistId() {
        return artistId;
    }


    public Integer getCollectionId() {
        return collectionId;
    }


    public Integer getTrackId() {
        return trackId;
    }


    public String getArtistName() {
        return artistName;
    }


    public String getCollectionName() {
        return collectionName;
    }

    public String getTrackName() {
        return trackName;
    }


    public String getCollectionCensoredName() {
        return collectionCensoredName;
    }

    public String getTrackCensoredName() {
        return trackCensoredName;
    }

    public Double getCollectionPrice() {
        return collectionPrice;
    }

    public Double getTrackPrice() {
        return trackPrice;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public String getCollectionExplicitness() {
        return collectionExplicitness;
    }


    public String getTrackExplicitness() {
        return trackExplicitness;
    }

    public Integer getDiscCount() {
        return discCount;
    }

    public Integer getDiscNumber() {
        return discNumber;
    }

    public Integer getTrackCount() {
        return trackCount;
    }

    public Integer getTrackNumber() {
        return trackNumber;
    }

    public Integer getTrackTimeMillis() {
        return trackTimeMillis;
    }

    public String getCountry() {
        return country;
    }

    public String getCurrency() {
        return currency;
    }

    public Boolean getIsStreamable() {
        return isStreamable;
    }

}

