package org.example.ghostage.itunesclient.data.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс реализующий  отображение
 * элементов API поиска для ITunes
 * (используется c Retrofit) - список песен
 */
public class SongsList{
    /**Размер результата*/
    @SerializedName("resultCount")
    @Expose
    private Integer resultCount;
    /**Список песен*/
    @SerializedName("results")
    @Expose
    private List<Song> results = null/*new ArrayList<Song>()*/;

    public SongsList(final List<Song> itunesSongs) {
        this.results = itunesSongs;
    }
    /**
     * Получение числа записей в полученном ответе
     * @return количество записей
     */
    public Integer getResultCount() {
        return resultCount;
    }
    /**
     * Получение списка песен
     * @return results - список песен
     */
    public List<Song> getResults() {
        return results;
    }


}
