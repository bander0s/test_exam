package org.example.ghostage.itunesclient;

import android.app.Application;

import org.example.ghostage.itunesclient.data.remote.model.Song;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс приложения для инициализации и хранения долгоживущих данных
 */
public class App extends Application{
    private final static String TAG = "App";

    public static final int MODE_LIST = 0;
    public static final int MODE_TABLE = 1;

    public static final String PREF_VIEW_MODE = "view_mode";

    public static int viewMode = MODE_LIST;
    public static ArrayList<Song> itunesTracks = new ArrayList<Song>();




}
