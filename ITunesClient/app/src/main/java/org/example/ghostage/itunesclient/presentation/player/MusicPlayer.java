package org.example.ghostage.itunesclient.presentation.player;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.example.ghostage.itunesclient.R;
import org.example.ghostage.itunesclient.data.local.MediastoreItem;
import org.example.ghostage.itunesclient.data.remote.model.Song;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;


/**
 * Медиаплейер, для проигрывания выбранной элемента списка
 */
public class MusicPlayer extends AppCompatActivity implements Button.OnClickListener, MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnPreparedListener {

    private MediaPlayer mediaPlayer;
    // кнопки управления воспроизведением
    private Button buttonPlay, buttonPrev, buttonNext;
    private Song mediastoreItem;
    // информация о медиафайле и временных метках воспроизведения
    private TextView curentTimeTextView, totalTimeTextView;
    private ImageView imageView; // просмотр обложки
    // управление процессом воспроизведения
    private ProgressBar progressBar, mediaProgressBar;
    private Toolbar toolbar;
    private CountDownTimer countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_player);

        setUIInLayout();

        mediaPlayer = new MediaPlayer();
        // проверка данных для родительского фрагмента
        Intent intent = getIntent();
        if (intent != null) {
            mediastoreItem = (Song) intent.getSerializableExtra("Object");
            toolbar.setTitle(mediastoreItem.getArtistName() + " - " + mediastoreItem.getTrackName());
            // загрузка обложки
            Picasso.with(getApplicationContext()).load(mediastoreItem.getArtworkUrl100()).into(imageView);
            //new LoadImage(imageView, progressBar).execute(mediastoreItem.getImageURLString());
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            try {
                mediaPlayer.setDataSource(mediastoreItem.getPreviewUrl());
                mediaPlayer.prepareAsync();
                mediaPlayer.setOnBufferingUpdateListener(this);
                mediaPlayer.setOnPreparedListener(this);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onPause() {
        if(mediaPlayer!=null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
                mediaPlayer.release();
            }
        }
        super.onPause();
    }

    /**
     * Отработка клика по кнопкам
     * @param view  -  кнопка
     */
    @Override
    public void onClick(View view) {
        int viewId = view.getId();
        switch(viewId){
            case R.id.playButton:
                if (!mediaPlayer.isPlaying()) {
                    mediaProgressBar.setProgress(0);
                    mediaPlayer.start();
                    buttonPlay.setBackgroundResource(R.drawable.icon_pause);
                    countDownTimer.start();
                } else {
                    mediaPlayer.pause();
                    buttonPlay.setBackgroundResource(R.drawable.icon_play);
                    countDownTimer.cancel();
                }
                break;
            case R.id.previousButton:
            case R.id.nextButton:
                countDownTimer.cancel();
                int position = mediaPlayer.getCurrentPosition();
                if (view == buttonPrev) {
                    position = position - (int)(position * 0.2);
                } else {
                    position /= 0.9;
                }
                mediaPlayer.seekTo(position);
                countDownTimer.start();
                break;
            default:
        }
    }

    /**
     * Определение интерфейса вызываемого обратного вызова, указывающее состояние
     * буферизации медиаресурса, передаваемого по сети
     * @param mediaPlayer - медиаплейер
     * @param i
     */
    @Override
    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
        Integer bufPos;
        if (i == 100) {
            bufPos = mediaProgressBar.getMax();
        } else {
            bufPos = i * mediaPlayer.getDuration() / 100;
        }
        mediaProgressBar.setSecondaryProgress(bufPos);
    }

    /**
     * MediaPlayer подготовил воспроизведение источника медиаданных
     * @param mediaPlayer - медиаплейер
     */
    @Override
    public void onPrepared(final MediaPlayer mediaPlayer) {
        mediaProgressBar.setMax((int)TimeUnit.MILLISECONDS.toSeconds(mediaPlayer.getDuration()));
        buttonPlay.setVisibility(View.VISIBLE);
        totalTimeTextView.setText(formatTime(mediaPlayer.getDuration()));
        countDownTimer = new CountDownTimer(mediaPlayer.getDuration() + 1000, 1000) {
            @Override
            public void onTick(long miliSeconds) {
                mediaProgressBar.setProgress((int)TimeUnit.MILLISECONDS.toSeconds(mediaPlayer.getCurrentPosition()));
                curentTimeTextView.setText(formatTime(mediaPlayer.getCurrentPosition()));
            }

            @Override
            public void onFinish() {
                mediaPlayer.stop();
                try {
                    mediaPlayer.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                countDownTimer.cancel();
                buttonPlay.setBackgroundResource(R.drawable.icon_replay);
            }
        };
    }

    /**
     * Ловит нажатия аппаратных клавиш
     * @param keyCode - скан код клавиш
     * @param event - событие, сгенерированное для клавиш
     * @return статус выполнения
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Проверяем какая кнопка была нажата и
        // проверяем, есть ли незаконченные действия
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            mediaPlayer.stop();
            mediaPlayer = null;
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * Функции получения id для UI  в форме и ининциализации
     * объектов для работы с ними в программе
     */
    protected void setUIInLayout() {
        // UI в панели вкладок
        toolbar = (Toolbar) findViewById(R.id.toolbarMusicPlayer);
        setSupportActionBar(toolbar);

        // UI in player
        progressBar = (ProgressBar) findViewById(R.id.imageProgressBar);
        imageView = (ImageView) findViewById(R.id.songImageView);

        // UI control
        buttonPlay = (Button) findViewById(R.id.playButton);
        buttonPlay.setVisibility(View.INVISIBLE);
        buttonPlay.setOnClickListener(this);

        buttonPrev = (Button) findViewById(R.id.previousButton);
        buttonPrev.setOnClickListener(this);

        buttonNext = (Button) findViewById(R.id.nextButton);
        buttonNext.setOnClickListener(this);

        curentTimeTextView = (TextView) findViewById(R.id.curentTimeTextView);
        totalTimeTextView = (TextView) findViewById(R.id.totalTimeTextView);

        mediaProgressBar = (ProgressBar)findViewById(R.id.mediaProgressBar);
    }

    /**
     * Форматируем время
     * @param miliSeconds время в милисеукундах
     * @return
     */
    protected String formatTime(long miliSeconds) {
        DateFormat dateFormat = new SimpleDateFormat("mm:ss");

        return dateFormat.format(miliSeconds);
    }
}
