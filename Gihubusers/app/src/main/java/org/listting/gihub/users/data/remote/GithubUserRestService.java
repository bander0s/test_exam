package org.listting.gihub.users.data.remote;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;
import org.listting.gihub.users.data.remote.model.User;
import org.listting.gihub.users.data.remote.model.UsersList;
/**
 * Интерфейс REST запросов для поиска пользователей
 * 50 - ограниечение числа получаемых пользователей
 */
public interface GithubUserRestService {

    @GET("/search/users?per_page=50")
    Observable<UsersList> searchGithubUsers(@Query("q") String searchTerm);

    @GET("/users/{username}")
    Observable<User> getUser(@Path("username") String username);
}
