package org.listting.gihub.users.data;


import java.io.IOException;
import java.util.List;

import rx.Observable;
import org.listting.gihub.users.data.remote.GithubUserRestService;
import org.listting.gihub.users.data.remote.model.User;

/**
 * Реализация поиска и обработки найденных пользователей
 */
public class UserRepositoryImpl implements UserRepository {
    protected static final String TAG = "UserListActivity";
    private GithubUserRestService githubUserRestService;

    /**
     * Конструктор реализации для работы с REST сервисом получения данных
     * @param githubUserRestService - сервис
     */
    public UserRepositoryImpl(GithubUserRestService githubUserRestService) {
        this.githubUserRestService = githubUserRestService;
    }

    /**
     * Поиск пользователя по указанной строке поиска
     * @param searchTerm - строка поиска
     * @return обсервер для работы с данными о пользователях
     */
    @Override
    public Observable<List<User>> searchUsers(final String searchTerm) {
        return Observable.defer(() -> githubUserRestService.searchGithubUsers(searchTerm)
                .concatMap(usersList -> Observable.from(usersList.getItems())
                .concatMap(user -> githubUserRestService.getUser(user.getLogin())).toList()
                )
                )

                .retryWhen(observable -> observable.flatMap(o -> {
                    if (o instanceof IOException) {
                        return Observable.just(null);
                    }
                    return Observable.error(o);
                }));
    }

}
