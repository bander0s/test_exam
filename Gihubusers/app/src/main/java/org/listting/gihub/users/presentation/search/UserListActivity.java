package org.listting.gihub.users.presentation.search;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import org.listting.gihub.users.R;
import org.listting.gihub.users.data.remote.model.User;
import org.listting.gihub.users.injection.Injection;

/**
 * Активтити со списком пользователей разделенным на три
 * диапазона по алфавиту
 */
public class UserListActivity extends AppCompatActivity implements UserListContract.View {
    protected static final String TAG = "UserListActivity";
    /**Указатель алфавитный на три вкладки + дополнительная для всех остальных,
     * начинающихся на другие симполы кроме алфавитных*/
    private static String[] alfaRanges = new String[]{"#","A-H","I-P","Q-Z"};
    private static List<User>[] userData;
    private static ViewPager mViewPager;
    private static SectionsPagerAdapter mSectionsPagerAdapter;
    private SearchView searchView;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private static UserListContract.Presenter userListPresenter;
    private static String term;
    private static Context ctx;
    private static int currentTab = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctx = getApplicationContext();
        setContentView(R.layout.activity);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        fab.setVisibility(View.GONE);
    }
    /**
     * Возобновление работы активити
     */
    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        userListPresenter = new UserSearchPresenter(Injection.provideUserRepo(), Schedulers.io(),
                AndroidSchedulers.mainThread());
        userListPresenter.attachView(this);
        term = term==null?"bell":term;
        query(term);
    }

    /**
     * Фильтрация полученного списка пользователей в соответствии с указанными критериями
     * TODO Есть возможность перевести обработку через Java 1.8 lambda
     * @param users полученный отсотированный список пользователей
     * @param termFilter - критерии фильтрации
     * @return отфильтрованные списки пользователей
     */
    private static List<User>[] filterUsers(List<User> users, String[] termFilter){
        List<User>[] result = new ArrayList[]{new ArrayList(),new ArrayList(),new ArrayList(),new ArrayList()};
        int i = 0, nextRange = 1;
        // Начальный граничный символ
        char termB = termFilter[nextRange].charAt(0);
        // Конечный граничный символ
        char termE = termFilter[nextRange].charAt(2);
        for(User u:users){
            String name = u.getName();
            name = name.substring(0,1).toUpperCase()+name.substring(1);
            if(!name.startsWith(String.valueOf(termB))
                    && termB >name.substring(0,1).toCharArray()[0]){
                result[nextRange-1].add(u);
                continue;
            }else{
                if(nextRange<termFilter.length) {
                    nextRange++;
                    if(nextRange<termFilter.length) {
                        termB = termFilter[nextRange].charAt(0);
                        termE = termFilter[nextRange].charAt(2);
                    }else {
                        if(nextRange<=termFilter.length) {
                            result[nextRange-1].add(u);
                            continue;
                        }
                        result[0].add(u);
                        continue;
                    }
                    result[nextRange - 1].add(u);
                }else{
                    // проверка для последнего диапазона
                    if(termE>=name.substring(0,1).toCharArray()[0]){
                        result[nextRange - 1].add(u);
                    }else {
                        // записываем в первый все остальные
                        result[0].add(u);
                    }
                }
            }
        }
        return result;
    }
    /**
     * Запрос поиска для получения пользователей
     * @param query строка поиска
     */
    private static void query(String query){
        //Предварительная обработка строки для приведения её в соответствие
        // с синтаксисом поискового запроса API
        query = query.replaceAll(" ", "+") + " in:name";
        if(userListPresenter != null)
            userListPresenter.search(query);
    }
    /**
     * Отображение списка пользователей
     * @param githubUserList - список пользователей
     */
    //@Override
    public static void showGithubSplitList(List<User> githubUserList) {
        // последующую фильтрацию для распределения по вкладкам
        userData = filterUsers(githubUserList, alfaRanges);
        // подключаем каждый список из массива к соответствующему фрагменту
        mViewPager.setCurrentItem(2);
        mViewPager.setCurrentItem(3);
    }

    /**
     * Получение списка пользователей Github
     * @param githubUserList - несортированный и нефильтрованный
     */
    @Override
    public void showGithubUsers(List<User> githubUserList) {
        showGithubSplitList(githubUserList);
    }

    /**
     * Отображение ошибок, в том числе и сетевых
     * @param message - текст ошибки
     */
    @Override
    public void showError(String message) {
        Toast t = Toast.makeText(ctx,getString(R.string.err_403), Toast.LENGTH_LONG);
        t.setGravity(Gravity.CENTER,0,0);
        t.show();
    }

    /**
     * Отображение прогресса загрузки
     */
    @Override
    public void showLoading() {
        ((PlaceholderFragment)mSectionsPagerAdapter.getItem(currentTab)).showLoading();
    }
    /**
     * Скрытие процесса загрузки
     */
    @Override
    public void hideLoading() {
        ((PlaceholderFragment)mSectionsPagerAdapter.getItem(currentTab)).hideLoading();
    }

    /**
     * Уничтожение активити
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(userListPresenter!=null) userListPresenter.detachView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_user_search, menu);
        final MenuItem searchActionMenuItem = menu.findItem(R.id.menu_search);
        searchView = (SearchView) searchActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                query(query);
                toolbar.setTitle(query);
                searchActionMenuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        searchActionMenuItem.expandActionView();
        return true;
    }


    /**
     * A {@link FragmentPagerAdapter} возвращает фрагмент, отображающий одну из вкладок
     * (для каждой свой экземпляр).
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Получение элемента-фрагмента для указанной вкладки
         * @param position - номер вкладки
         * @return фрагмент
         */
        @Override
        public Fragment getItem(int position) {
            // getItem вызывается для создания экземпляра фрагмента для каждой страницы.
            // Возвращает PlaceholderFragment (определенные как статический встроенный класс)
            return PlaceholderFragment.newInstance(position);
        }

        /**
         * Получение числа вкладок
         * @return число вкладок
         */
        @Override
        public int getCount() {
            // Показываем отдельные вкладки для каждого из алфавитных диапазонов.
            return alfaRanges.length;
        }

        /**
         * Получение название заголовка вкладки
         * @param position - номер вкладки
         * @return название
         */
        @Override
        public CharSequence getPageTitle(int position) {
            return alfaRanges[position];
        }
    }
    /**
     * Фрагмент - контейнер, содержащий простой вид.
     * @author ghost
     * @version 0.0.2
     */
    public static class PlaceholderFragment extends Fragment implements UserListContract.View{
        protected final static String TAG = "PlaceholderFragment";
        public static List<User> usersList;
        public UsersAdapter usersAdapter;
        private SearchView searchView;
        private Toolbar toolbar;
        private ProgressBar progressBar;
        private String number = "";
        public RecyclerView recyclerViewUsers;
        public TextView textViewErrorMessage;
        private Bundle args = new Bundle();


        /**Аргумент для идентификации секции*/
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Установка данных о пользователях в адаптер для отображения
         * @param users - данные
         */
        public void setUserData(List<User> users){
            usersAdapter.setItems(users);
            usersAdapter.notifyDataSetChanged();
        }
        /**
         * Возвращает новый эхкземпляр этого фрагмента, укзанного номера
         * вкладки
         * @param sectionNumber - номер вкладки
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putString(ARG_SECTION_NUMBER, String.valueOf(sectionNumber));
            fragment.setArguments(args);
            return fragment;
        }

        /**
         * Получение номера вкладки
         * @return номер вкладки
         */
        private String getNumber(){
            return this.number;
        }

        /**
         * Создание вида из xml ресурса
         * @param inflater
         * @param container -
         * @param savedInstanceState - карта с сохраненными состояниями
         * @return вид
         */
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            args = getArguments();
            number = args.getString(ARG_SECTION_NUMBER);
            View rootView = inflater.inflate(R.layout.fragment_user_search, container, false);
            progressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
            textViewErrorMessage = (TextView) rootView.findViewById(R.id.text_view_error_msg);
            recyclerViewUsers = (RecyclerView) rootView.findViewById(R.id.recycler_view_users);
            return rootView;
        }
        /**
         * Возобновление работы активити
         */
        @Override
        public void onResume() {
            super.onResume();
        }

        @Override
        public void onStart() {
            super.onStart();
            if(userData!=null) {
                this.usersAdapter = new UsersAdapter(userData[Integer.valueOf(number)], ctx);
                this.recyclerViewUsers.setVisibility(View.VISIBLE);
                this.progressBar.setVisibility(View.GONE);
                this.textViewErrorMessage.setVisibility(View.GONE);
                this.recyclerViewUsers.setAdapter(usersAdapter);
            }
        }

        public void showGithubUsers(List<User> githubUserList) {
            UserListActivity.showGithubSplitList(githubUserList);
        }
        public void showGithubFiltredUsers(List<User> githubUserList) {
            recyclerViewUsers.setVisibility(View.VISIBLE);
            textViewErrorMessage.setVisibility(View.GONE);
            setupAdapter(githubUserList); // TODO
        }

        private void setupAdapter(List<User> githubUserList) {
            usersAdapter.setItems(githubUserList);
            usersAdapter.notifyDataSetChanged();
        }

        @Override
        public void showError(String message) {
        }

        @Override
        public void showLoading() {
        }

        @Override
        public void hideLoading() {

        }
    }

}
