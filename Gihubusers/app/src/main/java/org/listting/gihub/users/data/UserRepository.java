package org.listting.gihub.users.data;


import java.util.List;

import rx.Observable;
import org.listting.gihub.users.data.remote.model.User;

/**
 * Интерфейс для пользователя репозитория
 */
public interface UserRepository {

    Observable<List<User>> searchUsers(String searchTerm);
}
