package org.listting.gihub.users;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

/**
 * Основной класс с методами для всего приложения
 */
public class App extends Application {
    protected static final String TAG = "App";


    /**
     * Добавление поддержки большого числа методов
     * @param base - контекст приложения
     */
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }
}
