package org.listting.gihub.users.presentation.search;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.listting.gihub.users.R;

/**
 * Место для вывода информации о конкретном пользователе в списке
 */
class UserViewHolder extends RecyclerView.ViewHolder {
    protected static final String TAG = "UserViewHolder";
    final TextView txtFollowersFolowing;
    final TextView txtViewName;
    final ImageView imgViewAvatar;
    /**Конструктор экземпляра*/
    UserViewHolder(View v ) {
        super(v);
        imgViewAvatar = (ImageView) v.findViewById(R.id.imageview_userprofilepic);
        txtViewName = (TextView) v.findViewById(R.id.textview_username);
        txtFollowersFolowing = (TextView) v.findViewById(R.id.textview_user_folowerrs_following);
    }
}
