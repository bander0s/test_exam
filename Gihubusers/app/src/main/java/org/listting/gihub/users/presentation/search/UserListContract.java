package org.listting.gihub.users.presentation.search;

import java.util.List;

import org.listting.gihub.users.data.remote.model.User;
import org.listting.gihub.users.presentation.base.MvpPresenter;
import org.listting.gihub.users.presentation.base.MvpView;

/**
 * Интерфейс для работы со списком пользователей
 */
interface UserListContract {

    interface View extends MvpView {
        void showGithubUsers(List<User> githubUserList);

        void showError(String message);

        void showLoading();

        void hideLoading();
    }

    interface Presenter extends MvpPresenter<View> {
        void search(String term);
    }
}
