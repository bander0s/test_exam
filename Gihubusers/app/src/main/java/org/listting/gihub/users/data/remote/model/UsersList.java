package org.listting.gihub.users.data.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Список пользователей и простейшие операции с ним
 * и возможности представления их для передачи через gson
 */
public class UsersList {

    @SerializedName("total_count")
    @Expose
    private Integer totalCount;
    @SerializedName("incomplete_results")
    @Expose
    private Boolean incompleteResults;
    @SerializedName("items")
    @Expose
    private List<User> items = new ArrayList<User>();

    public UsersList(final List<User> githubUsers) {
        this.items = githubUsers;
    }

    /**
     * Общее кол-во пользователей
     * @return число пользователей
     */
    public Integer getTotalCount() {
        return totalCount;
    }


    /**
     * Получение булевого статуса полученного результата,
     * который может быть неполным
     * @return статус
     */
    public Boolean getIncompleteResults() {
        return incompleteResults;
    }

    /**
     * Список всех элементов
     * @return элементы списка
     */
    public List<User> getItems() {
        return items;
    }

    /**
     * Сортировка списка по имени пользователя в прямом или обратном порядке
     * @param - признак реверса сортировки
     * @return - отсортированный список
     */
    public List<User> sortByName(boolean reverse) {
        Collections.sort(items, User.Comparators.NAME);
        return items;
    }
}
