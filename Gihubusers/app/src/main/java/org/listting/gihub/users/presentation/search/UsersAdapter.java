package org.listting.gihub.users.presentation.search;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import org.listting.gihub.users.R;
import org.listting.gihub.users.data.remote.model.User;

/**
 * Адаптер для отображения карточек пользователя
 */
class UsersAdapter extends RecyclerView.Adapter<UserViewHolder> {
    protected static final String TAG = "UsersAdapter";
    private final Context context;
    private List<User> items;

    /**
     * Конструктор списка с указанными пользователями
     * @param items - список пользователей
     * @param context - контекст приложения
     */
    UsersAdapter(List<User> items, Context context) {
        this.items = items;
        this.context = context;
    }

    /**
     * Развертывание элемента списка пользователей.
     * @param parent - родительская группа видов
     * @param viewType
     * @return проинициализированный экземпляр списка элементов
     */
    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_user, parent, false);
        return new UserViewHolder(v);
    }

    /**
     * Связывание вида м представлением данных для указанного
     * элемента в списке
     * @param holder представление
     * @param position - позиция отображаемого элемента
     */
    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        // получение пользователя из списка по индексу и заполнение соответствующих видов
        // информацией о пользователе
        User item = items.get(position);

        holder.txtFollowersFolowing.setText(item.getFollowers() + "/" + item.getFollowing());

        if (item.getName() != null) {
            holder.txtViewName.setText(item.getLogin() + " - " + item.getName());
        } else {
            holder.txtViewName.setText(item.getLogin());
        }
        // загружаем аватарку связанную с пользователем адресом
        Picasso.with(context).load(item.getAvatarUrl()).into(holder.imgViewAvatar);
    }

    /**
     * Получение количества элементов в списке
     * @return количество элементов
     */
    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    /**
     * Устанавливаем список пользователей для отображения
     * @param githubUserList список пользователей
     */
    void setItems(List<User> githubUserList) {
        this.items = githubUserList;
        notifyDataSetChanged();
    }
}