package org.listting.gihub.users.presentation.base;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Базоввый класс представления списка
 * @param <T>
 */
public class BasePresenter<T extends MvpView> implements MvpPresenter<T> {
    protected static final String TAG = "BasePresenter";
    private T view;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    /**
     * Подключение к виды из которого будет запущен поиск
     * Нужно для работы функций обратного вызова
     * @param mvpView
     */
    @Override
    public void attachView(T mvpView) {
        view = mvpView;
    }

    /**
     * Отключаемся от вида
     */
    @Override
    public void detachView() {
        compositeSubscription.clear();
        view = null;
    }

    /**
     * Прототип для получения вида.
     * Сделано для того, чтобы ьыла возможность подключения как, например, к активити, так и к фрагменту
     * @return
     */
    public T getView() {
        return view;
    }
    /**
     * Проверка, подключен ли вид
     */
    public void checkViewAttached() {
        if (!isViewAttached()) {
            throw new MvpViewNotAttachedException();
        }
    }
    /**
     * Признак, того что вид уже подключен
     * @return да/нет
     */
    private boolean isViewAttached() {
        return view != null;
    }
    /**
     * Подписка на обработку запроса поиска
     * @param subscription подписка
     */
    protected void addSubscription(Subscription subscription) {
        this.compositeSubscription.add(subscription);
    }
    /**
     * Исключение выводимое при при ошибке подключения. перехватываемое во время выполнения
     */
    public static class MvpViewNotAttachedException extends RuntimeException {
        public MvpViewNotAttachedException() {
            super("Please call Presenter.attachView(MvpView) before" + " requesting data to the Presenter");
        }
    }
}
