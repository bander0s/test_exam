package org.listting.gihub.users.presentation.search;

import java.util.Collections;
import java.util.List;

import rx.Scheduler;
import rx.Subscriber;
import org.listting.gihub.users.data.UserRepository;
import org.listting.gihub.users.data.remote.model.User;
import org.listting.gihub.users.presentation.base.BasePresenter;

/**
 * Поисковик пользователей
 */
class UserSearchPresenter extends BasePresenter<UserListContract.View> implements UserListContract.Presenter {
    protected static final String TAG = "UserSearchPresenter";

    private volatile int connectionCount = 0;
    private final Scheduler mainScheduler, ioScheduler;
    private UserRepository userRepository;

    /**
     * Экземпляр поисковика пользователей
     * @param userRepository
     * @param ioScheduler
     * @param mainScheduler
     */
    UserSearchPresenter(UserRepository userRepository, Scheduler ioScheduler, Scheduler mainScheduler) {
        this.userRepository = userRepository;
        this.ioScheduler = ioScheduler;
        this.mainScheduler = mainScheduler;
    }

    /**
     * Поиск указанной последовательности
     * @param term - строка для поиска
     */
    @Override
    public synchronized void search(String term) {
        checkViewAttached();
        getView().showLoading();
            addSubscription(userRepository.searchUsers(term).subscribeOn(ioScheduler).observeOn(mainScheduler)
                    .subscribe(new Subscriber<List<User>>() {
                                   @Override
                                   public void onCompleted() {

                                   }
                                   @Override
                                   public void onError(Throwable e) {
                                       getView().showError(e.getMessage());
                                       getView().hideLoading();
                                   }

                                   @Override
                                   public void onNext(List<User> users) {
                                       // Производим сортировку пользователей по алфавиту и
                                       Collections.sort(users,User.Comparators.NAME);
                                       getView().hideLoading();
                                       getView().showGithubUsers(users);
                                   }
                               }
                    )
            );
    }
}
