package org.listting.gihub.users.data.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Comparator;

/**
 * Основные поля пользователя предоставляемые API для
 * модели пользователя Github для чтения данных о пользователе
 */
public class User implements Comparator<User>, Comparable<User>{

    @SerializedName("login")
    @Expose
    private String login;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("avatar_url")
    @Expose
    private String avatarUrl;
    @SerializedName("gravatar_id")
    @Expose
    private String gravatarId;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("html_url")
    @Expose
    private String htmlUrl;
    @SerializedName("followers_url")
    @Expose
    private String followersUrl;
    @SerializedName("following_url")
    @Expose
    private String followingUrl;
    @SerializedName("gists_url")
    @Expose
    private String gistsUrl;
    @SerializedName("starred_url")
    @Expose
    private String starredUrl;
    @SerializedName("subscriptions_url")
    @Expose
    private String subscriptionsUrl;
    @SerializedName("organizations_url")
    @Expose
    private String organizationsUrl;
    @SerializedName("repos_url")
    @Expose
    private String reposUrl;
    @SerializedName("events_url")
    @Expose
    private String eventsUrl;
    @SerializedName("received_events_url")
    @Expose
    private String receivedEventsUrl;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("site_admin")
    @Expose
    private Boolean siteAdmin;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("company")
    @Expose
    private Object company;
    @SerializedName("blog")
    @Expose
    private String blog;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("email")
    @Expose
    private Object email;
    @SerializedName("hireable")
    @Expose
    private Object hireable;
    @SerializedName("bio")
    @Expose
    private String bio;
    @SerializedName("public_repos")
    @Expose
    private Integer publicRepos;
    @SerializedName("public_gists")
    @Expose
    private Integer publicGists;
    @SerializedName("followers")
    @Expose
    private Integer followers;
    @SerializedName("following")
    @Expose
    private Integer following;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public User() {
    }

    public User(final String userLogin, final String name, final String avatarUrl, final String bio) {
        this.login = userLogin;
        this.name = name;
        this.avatarUrl = avatarUrl;
        this.bio = bio;
    }
    public User(final String userLogin) {
        this.login = userLogin;
    }
    /**
     * @return логин
     */
    public String getLogin() {
        return login;
    }
    /**
     * @return идентификатор
     */
    public Integer getId() {
        return id;
    }
    /**
     * @return адрес аватарки
     */
    public String getAvatarUrl() {
        return avatarUrl;
    }
    /**
     * @return идентификатор аватарки
     */
    public String getGravatarId() {
        return gravatarId;
    }
    /**
     * @return адрес
     */
    public String getUrl() {
        return url;
    }
    /**
     * @return адрес html
     */
    public String getHtmlUrl() {
        return htmlUrl;
    }
    /**
     * @return адрес следующего
     */
    public String getFollowersUrl() {
        return followersUrl;
    }
    /**
     * @return адрес следуемого
     */
    public String getFollowingUrl() {
        return followingUrl;
    }
    /**
     * @return - адрес к сниплетам
     */
    public String getGistsUrl() {
        return gistsUrl;
    }
    /**
     * @return ссылка на зазвезденные проекты
     */
    public String getStarredUrl() {
        return starredUrl;
    }
    /**
     * @return ссылка на подписки
     */
    public String getSubscriptionsUrl() {
        return subscriptionsUrl;
    }
    /**
     * @return ссылка на организацию
     */
    public String getOrganizationsUrl() {
        return organizationsUrl;
    }
    /**
     * @return
     */
    public String getReposUrl() {
        return reposUrl;
    }
    /**
     * @return
     */
    public String getEventsUrl() {
        return eventsUrl;
    }
    /**
     * @return
     */
    public String getReceivedEventsUrl() {
        return receivedEventsUrl;
    }
    /**
     * @return тип аккаунта (бесплатный, приватный)
     */
    public String getType() {
        return type;
    }
    /**
     * @return сайт админа аккаунта
     */
    public Boolean getSiteAdmin() {
        return siteAdmin;
    }
    /**
     * @return имя известное в Github
     */
    public String getName() {
        return name;
    }
    /**
     * @return организация
     */
    public Object getCompany() {
        return company;
    }
    /**
     * @return заметки
     */
    public String getBlog() {
        return blog;
    }
    /**
     * @return местоположение
     */
    public String getLocation() {
        return location;
    }
    /**
     * @return электронная почта
     */
    public Object getEmail() {
        return email;
    }
    /**
     * @return
     */
    public Object getHireable() {
        return hireable;
    }
    /**
     * @return биография
     */
    public String getBio() {
        return bio;
    }
    /**
     * @return число публичных репозиториев
     */
    public Integer getPublicRepos() {
        return publicRepos;
    }
    /**
     * @return число сниплетов
     */
    public Integer getPublicGists() {
        return publicGists;
    }
    /**
     * @return число
     */
    public Integer getFollowers() {
        return followers;
    }
    /**
     * @return число
     */
    public Integer getFollowing() {
        return following;
    }
    /**
     * @return дата создания
     */
    public String getCreatedAt() {
        return createdAt;
    }
    /**
     * @return дата последнее обновление
     */
    public String getUpdatedAt() {
        return updatedAt;
    }
    /**
     * Сравнение с указанным объектом по строке имени
     * @param u - объект для сравнения
     * @return -1, 0, 1: что соответственно меньше, больше или равно
     */
    public int compareTo(User u){
        return (this.name).compareTo(u.name);
    }
    /**
     * Сортировка по сравнению с неинициализированным
     * @param d - первый объект для сравнения
     * @param d1 - второй объект для сравнения
     * @return -1, 0, 1: что соответственно меньше, больше или равно
     */
    public int compare(User d, User d1) {
        return d.id - d1.id;
    }

    /**
     * Установка имени пользователя
     * @param name - имя
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Критерии компаратора для пользователя
     */
    public static class Comparators {
        /**
         * Сравнение по имени
         * Если по какой-то причине name == null меняем на логин
         * (Случаи возврата null api Github - это довольно частое явление для старых акков)
         */
        public static Comparator<User> NAME = new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                if(o1.name == null){
                    o1.setName(o1.getLogin());
                }
                if(o2.name == null){
                    o2.setName(o2.getLogin());
                }
                return o1.name.compareTo(o2.name);
            }
        };
        /**
         * Сравнение по идентификатору
         */
        public static Comparator<User> ID = new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                return o1.id - o2.id;
            }
        };
        /**
         * Сравнение по имени и идентификатору
         */
        public static Comparator<User> NAMEANDID = new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                int i = o1.name.compareTo(o2.name);
                if (i == 0) { // детект равенства
                    // смотрим следующий критерий
                    i = o1.id - o2.id;
                }
                return i;
            }
        };
    }

    /**
     * Класс для сравнения объектов для класса User, использующий lambda Java 1.8
     * Но нельзя, так как минимальный API9 для класса Integer не поддерживает метода compare
     */
    public static class Comparators8 {
        public static final Comparator<User> NAME = (User o1, User o2) -> o1.name.compareTo(o2.name);
        public static final Comparator<User> ID = (User o1, User o2) -> Integer.compare(o1.id, o2.id);
        public static final Comparator<User> NAMEANDID = (User o1, User o2) -> NAME.thenComparing(ID).compare(o1, o2);
    }
}

