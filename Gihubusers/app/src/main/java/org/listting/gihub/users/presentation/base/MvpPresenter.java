package org.listting.gihub.users.presentation.base;

import android.view.View;

public interface MvpPresenter<V extends MvpView> {

    void attachView(V mvpView);

    void detachView();
}

