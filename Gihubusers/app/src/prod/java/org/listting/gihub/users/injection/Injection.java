package org.listting.gihub.users.injection;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import org.listting.gihub.users.data.UserRepository;
import org.listting.gihub.users.data.UserRepositoryImpl;
import org.listting.gihub.users.data.remote.GithubUserRestService;

/**
 * Класс для работы с github api
 * и запроса через сеть используя retrofit
 * Класс ветки для работы программы
 */
public class Injection {

    private static final String BASE_URL = "https://api.github.com";
    private static OkHttpClient okHttpClient;
    private static GithubUserRestService userRestService;
    private static Retrofit retrofitInstance;

    /**
     * Создаем экземпляр для работы с репозиторием пользователей, через который
     * можно получать и обрабатывать данные
     * @return
     */
    public static UserRepository provideUserRepo() {
        return new UserRepositoryImpl(provideGithubUserRestService());
    }

    /**
     * Подключаем пользовательские правила для использования API через Retrofit
     * @return - экземпляр сервиса с поддержкой функций retrofit
     */
    static GithubUserRestService provideGithubUserRestService() {
        if (userRestService == null) {
            userRestService = getRetrofitInstance().create(GithubUserRestService.class);
        }
        return userRestService;
    }

    /**
     * Создание клиента для получения данных и передачи их в обработку в retrofit
     * @return экземпляр http клиента для работы с http ресурсами
     */
    static OkHttpClient getOkHttpClient() {
        if (okHttpClient == null) {
            // добавляем логгер для отображения процесса получения результатов
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
            okHttpClient = new OkHttpClient.Builder().addInterceptor(logging).build();
        }

        return okHttpClient;
    }

    /**
     * Готовим retrofit к работе
     * @return экземпляр для работы с данными получаемые http-клиентом
     */
    static Retrofit getRetrofitInstance() {
        if (retrofitInstance == null) {
            Retrofit.Builder retrofit = new Retrofit.Builder().client(Injection.getOkHttpClient()).baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create());
            retrofitInstance = retrofit.build();

        }
        return retrofitInstance;
    }
}
